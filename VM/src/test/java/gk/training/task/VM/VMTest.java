package gk.training.task.VM;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import gk.training.task.entity.Item;

public class VMTest {
    
    public SimpleVM vm;
    
    @Before
    public void beforeInitialization() {
        vm = new SimpleVM();
        Map<Item, Integer> map = new HashMap<Item, Integer>();
        for(int i = 0; i < 15; i++) {
            Item item = new Item(i, "name", i * 10, "");
            map.put(item, 20);
        }
        vm.reloadGoods(map);
        vm.reloadGoods(map);
    }
    
    @Test
    public void testCountOfItems() {
        int count = vm.getCountOfItem(new Item(1, "name", 10, ""));
        assertEquals(40, count);
    }
    
    @Test
    public void testSellProcess1() {
        Item item = new Item(5, "name", 50, "");

        vm.sellItem(item);

        vm.updateMoneyInput(51);
        
        int count = vm.getCountOfItem(item);
        assertEquals(39, count);
        
        int money = vm.getCountOfAllMoney();
        assertEquals(50, money);
        
        int moneyIn = vm.getMoneyInput();
        assertEquals(0, moneyIn);
        
        int moneyOut = vm.getMoneyOutput();
        assertEquals(1, moneyOut);
    }
    
    @Test
    public void testSellProcess2() {
        Item item = new Item(5, "name", 50, "");
        vm.updateMoneyInput(51);
        vm.sellItem(item);        
        
        int count = vm.getCountOfItem(item);
        assertEquals(39, count);
        
        int money = vm.getCountOfAllMoney();
        assertEquals(50, money);
        
        int moneyIn = vm.getMoneyInput();
        assertEquals(0, moneyIn);
        
        int moneyOut = vm.getMoneyOutput();
        assertEquals(1, moneyOut);
    }
    
    @Test
    public void tesEncashment() {
        Item item = new Item(9, "name", 90, ""); 
        vm.updateMoneyInput(970);
        vm.sellItem(item);
        vm.encashment();
        
        int money = vm.getCountOfAllMoney();
        assertEquals(20, money);
        
        int moneyOut = vm.getMoneyOutput();
        assertEquals(880, moneyOut);
    }

}
