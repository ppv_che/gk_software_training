package gk.training.task.VM;

import gk.training.task.Comm.PointForRemotePayment;
import gk.training.task.Comm.VMComm;

public class CommVM extends SimpleVM implements VMComm {
  
  PointForRemotePayment pos;
  
  public void setPointForRemotePayment(PointForRemotePayment pos) {
    this.pos = pos;
  }
  
  public void enterCode(String code) {
    if (this.pos.checkCode(code)) {
      int money = this.pos.activateCode(code);
      this.updateMoneyInput(money);
      System.out.println("You have " + money + " credits.");
    } else {
      System.err.println("Code is not available");
    }
  }
}
