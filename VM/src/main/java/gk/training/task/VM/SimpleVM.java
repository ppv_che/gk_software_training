package gk.training.task.VM;

import java.util.HashMap;
import java.util.Map;

import gk.training.task.entity.Item;

/**
 * Vending machine realization
 * 
 * @author pprodan
 */
public class SimpleVM implements VM{

  private static int MIN_FOR_CHANGE = 20;

  private int allMoney;

  private Map<Item, Integer> itemCounts;

  private int moneyInput;

  private int moneyOutput;

  private Item tempItem;

  public SimpleVM() {
    this.itemCounts = new HashMap<Item, Integer>();
  }

  public int getCountOfAllMoney() {
    return this.allMoney;
  }

  public int getCountOfItem(Item item) {
    return this.itemCounts.get(item);
  }

  public void sellItem(Item item) {
    if (this.itemCounts.get(item) > 0) {
      // two modes depend on sequencing
      if (this.moneyInput >= item.getPrice()) {
        this.sell(item);
      } 
      else {
        this.tempItem = item;
      }
    } else {
      System.err.println("VM doesn't have this item");
    }
  }

  /**
   * method that give ability to set timeout of maximum busy time of VM put it
   * in else in sellGoods but it should call setter from other thread
   * 
   * @param item
   */
  private void waitMoney(Item item) {
    for (int i = 0; i < 180; i++) {
      try {
        System.err.println("Not enough credit!");
        Thread.sleep(1000);
        if (this.moneyInput >= item.getPrice()) {
          this.sell(item);
          return;
        }
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
    System.err.println("Time's up");
    this.cancel();
  }

  private void sell(Item item) {
    System.out.println("Bon Appetit!");
    this.itemCounts.put(item, this.itemCounts.get(item) - 1);
    this.moneyOutput = this.moneyInput - item.getPrice();
    this.moneyInput = 0;
    this.allMoney += item.getPrice();
    this.tempItem = null;
  }

  public void reloadGoods() {
    for (Map.Entry<Item, Integer> entry : this.itemCounts.entrySet()) {
      entry.setValue(20);
    }
  }

  public void reloadGoods(Map<Item, Integer> mapItems) {
    for (Map.Entry<Item, Integer> entry : mapItems.entrySet()) {
      Item item = entry.getKey();
      int amount = entry.getValue();
      if (this.itemCounts.containsKey(item)) {
        this.itemCounts.put(item, (amount + this.itemCounts.get(item)));
      } else {
        this.itemCounts.put(item, amount);
      }
    }
  }

  public void encashment() {
    if (this.allMoney > MIN_FOR_CHANGE) {
      this.allMoney = MIN_FOR_CHANGE;
    } else {
      System.err.println("Amount of money in VM is less than minimum required amount for change");
    }
  }

  public void cancel() {
    this.moneyOutput = this.moneyInput;
    this.moneyInput = 0;
    this.tempItem = null;
  }

  public int getMoneyOutput() {
    return this.moneyOutput;
  }

  public void setMoneyOutput(int moneyOutput) {
    this.moneyOutput = moneyOutput;
  }

  public static int getMIN_FOR_CHANGE() {
    return MIN_FOR_CHANGE;
  }

  public static void setMIN_FOR_CHANGE(int min_for_change) {
    MIN_FOR_CHANGE = min_for_change;
  }

  public int getMoneyInput() {
    return this.moneyInput;
  }

  public void updateMoneyInput(int moneyInput) {
    if (moneyInput > 0) {
      this.moneyInput += moneyInput;
      if ((this.tempItem != null) && (this.tempItem.getPrice() <= this.moneyInput)) {
        this.sell(this.tempItem);
      } 
    } else {
      System.err.println("Incorrect data");
    }
  }

  public void removeMoneyInput() {
    if (this.moneyInput > 0) {
      this.allMoney += this.moneyInput;
      this.moneyInput = 0;
    }
  }
}
