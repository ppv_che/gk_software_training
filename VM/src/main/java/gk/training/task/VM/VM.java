package gk.training.task.VM;

import java.util.Map;

import gk.training.task.entity.Item;

public interface VM {
    
    public int getCountOfAllMoney();
    
    public int getCountOfItem(Item item);
    
    public void sellItem(Item item);
    
    public void reloadGoods();
    
    public void reloadGoods(Map<Item, Integer> mapItems);
    
    public void encashment();
    
    public void cancel();
    
    public int getMoneyOutput();

    public void setMoneyOutput(int moneyOutput);
    
    public int getMoneyInput();
    
    public void updateMoneyInput(int moneyInput);
    
    public void removeMoneyInput();

}