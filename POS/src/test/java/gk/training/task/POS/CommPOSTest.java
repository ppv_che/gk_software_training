package gk.training.task.POS;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import gk.training.task.VM.CommVM;
import gk.training.task.entity.Item;

public class CommPOSTest {
  
  CommPOS pos = new CommPOS();
  CommVM vm = new CommVM();
  
  @Before
  public void beforeInit() {
    Map<Item, Integer> map = new HashMap<Item, Integer>();
    for(int i = 0; i < 15; i++) {
        Item itemForMap = new Item(i, "testName", i * 10, "");
        map.put(itemForMap, 20);
    }
    vm.reloadGoods(map);
    vm.setPointForRemotePayment(pos);
  }
  
  @Test
  public void testCodeInVM() {
    String code = pos.generateUniqueCodeForCustomer(25);
    
    boolean check = pos.checkCode(code);
    assertTrue(check);
  }
  
  @Test
  public void testUseCodeInVM() {
    Item item = new Item(3, "testName", 30, "");
    String code = pos.generateUniqueCodeForCustomer(25);
    vm.enterCode(code);
    vm.sellItem(item);
    vm.updateMoneyInput(10);
    
    int itemCount = vm.getCountOfItem(item);
    assertEquals(19, itemCount);
    
    int moneyOut = vm.getMoneyOutput();
    assertEquals(5, moneyOut);
    
    boolean check = pos.checkCode(code);
    assertFalse(check);
    
    int money = pos.getCurrentMoneyAmount();
    assertEquals(25, money);
  }
  
  @Test
  public void testReturn() {
    String code = pos.generateUniqueCodeForCustomer(25);
    vm.enterCode(code);
    vm.updateMoneyInput(10);
    vm.cancel();
    
    int moneyOut = vm.getMoneyOutput();
    assertEquals(35, moneyOut);
  }

}
