package gk.training.task.POS;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import gk.training.task.Comm.PointForRemotePayment;
import gk.training.task.entity.Code;

/**
 * Point for sail realization with ability to communicate with VM
 * 
 * @author pprodan
 *
 */
public class CommPOS extends SimplePOS implements PointForRemotePayment {
  
  Map<String, Integer> activeCodesToMoney = new HashMap<String, Integer>();
  
  List<Code> historyCodes = new ArrayList<Code>();

  private String generateCode() {
    int codeNum = Math.abs((int) (Math.random() * 10000));
    String code = "" + codeNum;
    while (code.length() < 4) {
      code = 0 + code;
    }
    return "" + code;
  }

  public String generateUniqueCodeForCustomer(int money) {
    String code = "";
    if (money > 0) {
      do {
        code = this.generateCode();
      } while (this.activeCodesToMoney.containsKey(code));
      this.updateCurrentMoneyAmount(money);
      this.activeCodesToMoney.put(code, money);
    } else {
      System.err.println("Incorrect data");
    }
    return code;
  }

  public boolean checkCode(String code) {
    return this.activeCodesToMoney.containsKey(code);
  }

  public int activateCode(String code) {
    if (this.checkCode(code)) {
      int money = this.activeCodesToMoney.get(code);
      this.activeCodesToMoney.remove(code);
      this.historyCodes.add(new Code(code, money));
      return money;
    } else {
      return 0;
    }
  }

  public void returnByCode(String code) {
    if (this.activeCodesToMoney.containsKey(code)) {
      int credits = this.activeCodesToMoney.get(code);
      int money = this.getCurrentMoneyAmount();
      if (money >= credits) {
        this.updateCurrentMoneyAmount(-credits);
      }
      this.activeCodesToMoney.remove(code);
    } else {
      System.err.println("Code is not exist in available list.");
    }
  }

  public List<Code> getHistoryCodes() {
    return this.historyCodes;
  }

}
