package gk.training.task.POS;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import gk.training.task.entity.Item;

/**
 * Point of sale realization
 * @author pprodan
 *
 */
public class SimplePOS implements POS {
    
    //field that define minimal amount of money for change which we keep after encashment 
    private static int MIN_CASH = 100; 
    
    private int money;
    
    private List<Item> itemList;
    
    //maps where we keep history of revenue, encashment and sales bound with time(use them in report)
    private Map<Timestamp, Integer> revenueMap;
    
    private Map<Timestamp, Integer> encashmentMap;

    private Map<Timestamp, Item> salesMap;

    public SimplePOS() {
        this.itemList = new ArrayList<Item>();
        this.salesMap = new TreeMap<Timestamp, Item>();
        this.revenueMap = new TreeMap<Timestamp, Integer>();
        this.encashmentMap = new TreeMap<Timestamp, Integer>();
    }

    public int getCurrentMoneyAmount() {
        return money;
    }
    
    public void updateCurrentMoneyAmount(int money) {
        if (money > 0) {
            this.money += money;
        } else if (this.money > Math.abs(money)) {
          this.money += money;
        }
    }

    public List<Item> getListOfAvailbaleSaleItems() {
        return this.itemList;
    }

    public void sellItem(Item item) {
        if (itemList.contains(item)) {
            //set reference to identical item from list for correct indication available state 
            item = itemList.get(itemList.indexOf(item));
            this.money += item.getPrice();
            this.itemList.remove(itemList.get(itemList.indexOf(item)));
            Timestamp timeSales = createUniqueTime(this.salesMap.keySet());
            this.salesMap.put(timeSales, item);
            Timestamp timeRevenue = createUniqueTime(this.revenueMap.keySet());
            this.revenueMap.put(timeRevenue, item.getPrice());
        } else {
            System.err.println("POS doesn't have this item");
        }
    }

    public void returnSoldItem(Item item) {
        if (this.salesMap.containsValue(item)) {
            if (this.money >= item.getPrice()) {
                this.money -= item.getPrice();
                this.itemList.add(item);
                Timestamp time = createUniqueTime(this.revenueMap.keySet());
                this.revenueMap.put(time, item.getPrice()*(-1));
                for(Map.Entry<Timestamp, Item> entry : this.salesMap.entrySet()) {
                    if (entry.getValue().equals(item)) {
                        this.salesMap.remove(entry.getKey());
                        break;
                    }
                }
            } else {
                System.err.println("POS doesn't have enough money for making return operation");
            }
        } else {
            System.err.println("POS doesn't have this item in sales history");
        }
    }

    public void encashment() {
        if (this.money > MIN_CASH) {
            Timestamp time = createUniqueTime(encashmentMap.keySet());
            int encash = this.money - MIN_CASH;
            this.encashmentMap.put(time, encash);
            this.money = MIN_CASH;
        } else {
            System.err.println("Amount of money in POS is less than minimum required amount for change");
        }
    }

    public void storeItems(List<Item> items) {
        itemList.addAll(items);
    }

    public String report(Timestamp from, Timestamp to) {
        
        StringBuilder resultBuilder = new StringBuilder();
        
        for(Timestamp time : salesMap.keySet()) {
            if ((time.getTime() >= from.getTime())&&(time.getTime() <= to.getTime())) {
                resultBuilder.append(time + " : Item " + salesMap.get(time).getId() + " " + salesMap.get(time).getName() + " was saled.\n");
            }
        }
        for(Timestamp time : encashmentMap.keySet()) {
            if ((time.getTime() >= from.getTime())&&(time.getTime() <= to.getTime())) {
                resultBuilder.append(time + " : Encashed " + encashmentMap.get(time) + ".\n");
            }
        }
        int revenueForPeriod = 0;
        for(Timestamp time : revenueMap.keySet()) {
            if ((time.getTime() >= from.getTime())&&(time.getTime() <= to.getTime())) {
                revenueForPeriod += revenueMap.get(time);
            }
        }
        resultBuilder.append("Revenue for period: " + revenueForPeriod + ".\n");
        
        return resultBuilder.toString();
    }
    
    public int countAvailableItem(Item itemCheck) {
        int result = 0;
        for(Item item : this.itemList) {
            if (item.equals(itemCheck)) {
                result++;
            }
        }
        return result;
    }
    
    private static Timestamp createUniqueTime(Set<Timestamp> setTime) {
        Timestamp time = new Timestamp(new Date().getTime());
        while(setTime.contains(time)) {
            time.setTime(time.getTime() + 1);
        }
        return time;
    }

    public static int getMIN_ENCASH() {
        return MIN_CASH;
    }

    public static void setMIN_ENCASH(int min_encash) {
        MIN_CASH = min_encash;
    }
    
}
