package gk.training.task.Comm;

import java.util.List;

import gk.training.task.entity.Code;

public interface PointForRemotePayment {

  public String generateUniqueCodeForCustomer(int money);
  
  public boolean checkCode(String code);
  
  public int activateCode(String code);
  
  public void returnByCode(String code);
  
  public List<Code> getHistoryCodes();

}
