package gk.training.task.Comm;

public interface VMComm {
  
  public void setPointForRemotePayment(PointForRemotePayment pos);
  
  public void enterCode(String code);

}