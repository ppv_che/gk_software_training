package gk.training.task.entity;

public class Code {
    
    private String code;
    
    private int money;
    
    public Code(String code, int money) {
        this.code = code;
        this.money = money;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    @Override
    public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = prime * result + ((code == null) ? 0 : code.hashCode());
      result = prime * result + money;
      return result;
    }

    @Override
    public boolean equals(Object obj) {
      if (this == obj)
        return true;
      if (obj == null)
        return false;
      if (getClass() != obj.getClass())
        return false;
      Code other = (Code) obj;
      if (code == null) {
        if (other.code != null)
          return false;
      } else if (!code.equals(other.code))
        return false;
      if (money != other.money)
        return false;
      return true;
    }

}
